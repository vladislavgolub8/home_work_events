let elemLeft;
let elemTop;
let maxElemLeft;
let maxElemTop;
let elem;

elem = document.getElementById("runner");

maxElemLeft = document.documentElement.clientWidth - elem.offsetWidth;
maxElemTop = document.documentElement.clientHeight - elem.offsetHeight;

elem.onmousemove = run;

function run() {
    elemLeft = Math.random() * maxElemLeft;
    elem.style.left = elemLeft + "px";
    elemTop = Math.random() * maxElemTop;
    elem.style.top = elemTop + "px";
}
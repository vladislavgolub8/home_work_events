let child; 
let childContent;

window.onload = function () {
    childContent = document.getElementsByClassName('child_content');
    child = document.getElementsByClassName('child');
    hideChildContent(1);
}
function hideChildContent(a) {
    for (let i = a;i < childContent.length;i++) {
        childContent[i].classList.remove('show');
        childContent[i].classList.add("hide");
        child[i].classList.remove('white_activ');
        
    }
}
document.getElementById('container').onclick = function (event) {
    let target = event.target;
    if (target.className == 'child') {
        for (let i = 0;i < child.length;i++) {
            if (target == child[i]) {
                showChildContent(i);
                break;
            }
        }
    }
}
function showChildContent(b) {
    if (childContent[b].classList.contains('hide')) {
        hideChildContent(0);
        child[b].classList.add('white_activ');
        childContent[b].classList.remove('hide');
        childContent[b].classList.add('show');
    }
}